import {combineReducers, createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import loginPageReducer from "./Reducers/loginReducer"
import authReducer from "./Reducers/authReducer";
import profilePageReducer from "./Reducers/profilePageReducer";
import dialogPageReducer from "./Reducers/dialogPageReducer";
import usersPageReducer from "./Reducers/usersPageReducer";

const superReducer = combineReducers({
    profilePage: profilePageReducer,
    dialogPage: dialogPageReducer,
    loginPage: loginPageReducer,
    auth: authReducer,
    users: usersPageReducer,
    logOut: loginPageReducer
});

const store = createStore(superReducer,applyMiddleware(thunk));

export default store;
