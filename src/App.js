import React from 'react';
import './App.css';
import LoginPage from "./Components/Login/LoginPage";
import {BrowserRouter} from "react-router-dom";
import {Route, Switch} from "react-router";
import DialogsPage from "./Components/DialogsPage/DialogsPage";
import ProfilePage from "./Components/ProfilePage/ProfilePage";
import AllUsersPage from "./Components/AllUsersPage/AllUsersPage";
import NotFoundPage from "./Components/NotFoundPage/NotFoundPage";
import UserPage from "./Components/UserPage/UserPage";

const App = () => {
    return (

        <BrowserRouter>

            <div className="app">
                <Switch>
                    <Route exact path="/" render={() => <LoginPage/>}/>
                    <Route exact path="/login" render={() => <LoginPage/>}/>
                    <Route exact path="/dialogs" render={() => <DialogsPage/>}/>
                    <Route exact path="/profile" render={() => <ProfilePage/>}/>
                    <Route exact path="/users" render={() => <AllUsersPage/>}/>
                    <Route exact path="/users/:userId?" render={() => <UserPage/>}/>
                    <Route path="/" render={() => <NotFoundPage/>}/>
                </Switch>
            </div>
        </BrowserRouter>

    );
};

export default App;
