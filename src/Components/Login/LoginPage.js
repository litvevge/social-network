import React from 'react';
import style from "./LoginPage.module.css"
import {connect} from "react-redux";
import {login} from "../../Reducers/loginReducer";
import {statuses} from "../../Reducers/usersPageReducer";
import {Redirect} from "react-router";

const LoginPage = (props) => {

    if (props.isLoggedIn) {
        return <Redirect  to='/profile'/>
    }

    let loginRef = React.createRef();
    let passwordRef = React.createRef();
    let rememberMeRef = React.createRef();

    const onLoginClick = ( () => {
        props.login && props.login(loginRef.current.value, passwordRef.current.value, rememberMeRef.current.checked)
     }
    );

    // if (props.isLoggedIn) {
    //     return <Redirect to="/profile" />
    // }
let errorMessageBlock = props.status === statuses.ERROR &&
        <div className={style.error}>{props.message}</div>;


    return (
        <div className={style["page-width"]}>
            <div className={style["login-page"]}>
                <div className={style.container}>
                    <div className={style["login-page"]}>
                        <span className={style["span-login"]}>Login</span>
                        <span className={style["span-login"]}>Registration</span>
                    </div>
                    <div className={style.container}>
                        <div className={style["login-items"]}>
                            <span>Login</span>
                            <input className={style.inputs} type="email" ref={loginRef} defaultValue='essedger@gmail.com'/>
                        </div>
                        <div className={style["login-items"]}>
                            <span>Password</span>
                            <input className={style.inputs} type="password" ref={passwordRef} defaultValue='12345678'/>
                        </div>
                        <div className={style["flexy-inputs"]}>
                            <div>
                                <input className={style["input-green"]} type="checkbox" ref={rememberMeRef}
                                       checked={props.checkbox}/><span>remember me</span>
                            </div>
                            <div>
                                <button
                                    disabled={props.status.INPROGRESS}
                                    onClick={() => {
                                    onLoginClick();
                                }}
                                className={style["input-green"]}
                                >Login
                                </button>
                            </div>
                        </div>
                        {errorMessageBlock}
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        isAuth : state.auth.isAuth,
        status: state.loginPage.status,
        message: state.loginPage.message,
        captchaURL: state.loginPage.captchaURL,

        email: state.loginPage.email,
        password: state.loginPage.currentPassword,
        checkbox: state.loginPage.rememberMe,
        isLoggedIn: state.auth.isLoggedIn

    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        login: (email, password, rememberMe) => {
            dispatch(login(email, password, rememberMe))
        }
    }
};
const ConnectLoginPage = connect(mapStateToProps, mapDispatchToProps)(LoginPage);

export default ConnectLoginPage;

