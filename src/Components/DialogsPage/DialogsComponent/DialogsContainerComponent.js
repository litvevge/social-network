import {connect} from "react-redux";
import {SET_CURRENT_USER} from "../../../Reducers/dialogPageReducer";
import DialogsComponent from "./DialogsComponent";

const mapStateToProps = (state) => {
    return {
        dialogPage: state.dialogPage
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        currentUserId: (id) => {
            dispatch({
                type:SET_CURRENT_USER,
                id: id
            })
        }
    }
};

const ConnectDialogs = connect(mapStateToProps, mapDispatchToProps)(DialogsComponent);

export default ConnectDialogs;
