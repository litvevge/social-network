import React from 'react';
import DialogsMessageComponent from "../DialogsMessageComponent/DialogsMessageComponent";
import style from "./DialogsComponent.module.css";
import DialogsAuthorComponent from "../DialogsAuthorComponent/DialogsAuthorComponent";

const DialogsComponent = (props) => {
    let messageDialogsList = props.dialogPage.dialogs.messages.map ((message) => {
        return <DialogsMessageComponent message={message} avatar={props.dialogPage.dialogs.messages.avatar}/>
    } );

    return (
        <div>
            <span className={style["name-span"]}>DIALOGS</span>
            <div className={style["content-wrapper"]}>
                <div>
                    <DialogsAuthorComponent dialogs={props.dialogPage}/>
                </div>
                <div>
                    {messageDialogsList}
                </div>
            </div>
        </div>
    );
};

export default DialogsComponent;
