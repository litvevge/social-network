import React from 'react';
import style from "./DialogsMessageComponent.module.css"

const DialogsMessageComponent = (props) => {
debugger;
        return (
            <div className={style["content-wrapper"]}>
                <div>
                    <img className={style["message-img"]} src={props.message.avatar} alt="avatar"/>
                </div>
                <div>
                    {props.message.message}
                </div>
            </div>
        );
    };

export default DialogsMessageComponent;
