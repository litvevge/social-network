import React from 'react';
import style from "./DialogsAuthorComponent.module.css";

const DialogsAuthorComponent = (props) => {

    //let setCurrentUser = props.dialogPage.dialogs.setCurrentUser;
    let users = props.dialogs.dialogs.users;
    //let currentUserId = props.dialogs.dialogs.currentUserId;
    let authorDialogsList = users.map(user => {
        // let isUserSelected = currentUserId === user.id;
        // let cssClasses = isUserSelected ? style.selected : "";
        return <li onClick={ () => props.currentUserId(user.id)}> {user.name}</li>
            //className={cssClasses}
                   // onClick={() => {
                   //     setCurrentUser(user.id)}}
    });

    return (
        <div className={style["content-wrapper"]}>
            <ul>
                <span className={style["people-list"]}>{authorDialogsList}</span>
            </ul>
        </div>
    );
};

export default DialogsAuthorComponent;
