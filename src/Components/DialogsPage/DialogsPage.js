import React from 'react';
import SidebarComponent from "../SidebarComponent/SidebarComponent";
import style from "./DialogsPage.module.css";
import ConnectDialogs from "./DialogsComponent/DialogsContainerComponent";
import HeaderContainer from "../Header/HeaderContainer";

const DialogsPage = () => {
    return (
      <div className={style["page-width"]}>
        <HeaderContainer/>
        <div className={style["content-wrapper"]}>
            <div>
              <SidebarComponent />
            </div>
            <div>
              <ConnectDialogs />
            </div>
        </div>

      </div>
    );
  };

export default DialogsPage;
