import React from "react";
import withRouter from "react-router/es/withRouter";
import axiosInstance from "../../dal/axios-instance";

class UserPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: null,
            editMode: false,
            me: null,
            isOwner: true
        };

    }

    componentDidMount() {
        let userIdFromUrl = this.props.match.params.userId;
        let profilePromise = axiosInstance.get('profile/' + userIdFromUrl)
            .then((response) => {
                this.setState({profile:response.data});
            });
        let mePromise = axiosInstance.get('auth/me')
            .then((response) => {
                this.setState({me:response.data});
            });
        Promise.all([mePromise, profilePromise]).then(() => {
            let {profile, me} = this.state;
            if (!!me && !!profile && me.id === profile.id){
                this.setState({isOwner : true});
            }
        })
    }

    render() {
        let {isOwner} = this.state;



        if (this.state.profile) {
            return <div>
                <img src={this.state.profile.photos.large} alt={this.state.profile.photos.small}/>

                <div>UserID: {this.state.profile.userId}</div> {isOwner && <button>Edit</button>}
                {/*<div>Looking for a job: {this.state.profile.lookingForAJob}</div>*/}
                <div>Fullname: {this.state.profile.fullName}</div>{isOwner && <button>Edit</button>}
                <div>About me: {this.state.profile.aboutMe}</div>{isOwner && <button>Edit</button>}
                <div>Contacts</div>
                <div>
                    {
                        Object.keys(this.state.profile.contacts).map(key => {
                            return <div>{key} : {this.state.profile.contacts[key]}{isOwner && <button>Edit</button>}</div>
                        })
                    }
                </div>
            </div>
        }
        else {
            return <div>
                Loading...
            </div>
        }
    }
}

export default withRouter(UserPage);

