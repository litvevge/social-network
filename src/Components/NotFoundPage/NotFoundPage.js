import React from "react";
import style from './NotFoundPage.module.css';
import NavLink from "react-router-dom/es/NavLink";

const NotFoundPage = () => {
    return (
        <>
            <div className={style.wrapper}>
                <img src='https://d3nuqriibqh3vw.cloudfront.net/media-vimeo/70533052.jpg' alt="not found"/>
            </div>
            <div className={style.wrapper}>
                <NavLink to='/'>Return</NavLink>
            </div >
        </>
    );
};

export default NotFoundPage;
