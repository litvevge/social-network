import React from 'react';
import SidebarComponent from "../SidebarComponent/SidebarComponent";
import style from "./AllUsersPage.module.css";
import UsersComponent from "./AllUsersComponent/AllUsersComponent";
import HeaderContainer from "../Header/HeaderContainer";

const AllUsersPage = () => {
    return (
      <div className={style["page-width"]}>
        <HeaderContainer/>
        <div className={style["content-wrapper"]}>
            <div>
              <SidebarComponent />
            </div>
            <div>
                <UsersComponent/>
            </div>
        </div>

      </div>
    );
  };

export default AllUsersPage;
