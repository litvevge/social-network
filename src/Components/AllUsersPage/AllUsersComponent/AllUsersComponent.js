import React from 'react';
import style from "./AllUsersComponent.module.css";
import {NavLink} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {getUsers, statuses} from "../../../Reducers/usersPageReducer";

const AllUsersComponent = ({users = [], status, getUsers}) => {
    if (status === statuses.NOT_INITIOLIZED) {
        getUsers();
        return <span>...</span>
    }


    return (

        <div className={style["content-wrapper"]}>
            {!users.length && <span>users not found</span>}
            {
                users.map(user => <div className={style.user}>
                    <div>
                        <img src={user.photo} alt=""/>
                    </div>
                    <div>
                        <NavLink to={`users/${user.id}`}>{user.name}</NavLink>
                    </div>
                </div>)
            }
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        users: state.users.items,
        status: state.users.status
    }
};
const mapDispatchToProps = (dispatch) => ({
    getUsers: () => {
        dispatch(getUsers())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AllUsersComponent);
