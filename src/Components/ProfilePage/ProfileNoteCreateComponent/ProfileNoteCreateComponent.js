import React from 'react';
import style from './ProfileNoteCreateComponent.module.css';
import {connect} from "react-redux";
import {addWallPost, setCurrentPostText} from "../../../Reducers/profilePageReducer";

const ProfileNoteCreateComponent = (props) => {

    let wallText = React.createRef();
    return (
        <div>
            <div>
                <span className={style["span-note"]}>My notes</span>
            </div>
            <div>
                <div className={style["text-area"]}>
                    <textarea ref={wallText} name="wall-notes" cols="55" rows="2"> </textarea>
                </div>
                <div className={style["send-button"]}>
                    <button onClick={() => {
                        props.addPost(wallText.current.value)
                    }} className={style.button}>Send</button>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (store) => {

    return {
        profilePage: store.profilePage.wallPosts
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        addPost: (text) => {
            dispatch(addWallPost(text));
        },
        currentPostText: (text) => {
            dispatch(setCurrentPostText(text))
        }
    }
};
const ConnectProfileNotesCreate = connect(mapStateToProps, mapDispatchToProps)(ProfileNoteCreateComponent);

export default ConnectProfileNotesCreate;
