import React from 'react';
import style from './ProfileImageComponent.module.css';

const ProfileImageComponent = (props) => {
    return (
      <div >
          <img className={style["profile-img"]} src={props.profilePage.avatar} alt="profileImage"/>
      </div>
    );
  };

export default ProfileImageComponent;
