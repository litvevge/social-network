import React from 'react';
import ProfileWallpaperComponent from "../ProfileWallpaperComponent/ProfileWallpaperComponent";
import ProfileImageComponent from "../ProfileImageComponent/ProfileImageComponent";
import ProfileInfoComponent from "../ProfileInfoComponent/ProfileInfoComponent";
import style from './ProfileComponent.module.css';
import {connect} from "react-redux";
import {Redirect} from "react-router";

const ProfileComponent = (props) => {

    if (!props.isLoggedIn) {
        return <Redirect to="/login" />
    }

    return (
        <div>
            <ProfileWallpaperComponent {...props}/>
            <div className={style["profile-content"]}>
                <ProfileImageComponent {...props}/>
                <ProfileInfoComponent {...props}/>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {

    return {
        profilePage: state.profilePage.profileInfo,
        isLoggedIn: state.auth.isLoggedIn
    }
};
const mapDispatchToProps = () => {
    return {

    }
};
const ConnectProfile = connect(mapStateToProps, mapDispatchToProps)(ProfileComponent);

export default ConnectProfile;
