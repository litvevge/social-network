import React from 'react';
import style from './ProfileWallElementComponent.module.css';

const ProfileWallElementComponent = (props) => {

    return (
        <div>
            <div className={style["content-wrapper"]}>
                <div>
                    <img className={style["wall-avatar"]} src={props.avatar} alt="avatar"/>
                </div>
                <div className={style["content-wrapper"]}>
                    <p>{props.post}</p>
                </div>
            </div>
        </div>
    );
};

export default ProfileWallElementComponent;
