import React from 'react';
import style from './ProfileInfoComponent.module.css';

const ProfileInfoComponent = (props) => {
        return (
            <div>
                <div className={style["profile-content"]}>
                    <div>
                        <span>First name : {props.profilePage.firstsName}</span>
                    </div>
                    <div>
                        <span>Second name : {props.profilePage.secondName}</span>
                    </div>
                    <div>
                        <span>Birthday : {props.profilePage.birthday}</span>
                    </div>
                    <div>
                        <span>City : {props.profilePage.city}</span>
                    </div>
                    <div>
                        <span>Education : {props.profilePage.education}</span>
                    </div>
                    <div>
                        <span>Website : {props.profilePage.website}</span>
                    </div>
                </div>
            </div>
        );
    };

export default ProfileInfoComponent;
