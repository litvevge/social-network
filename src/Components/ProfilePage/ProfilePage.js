import React from 'react';
import SidebarComponent from "../SidebarComponent/SidebarComponent";
import ProfileComponent from "./ProfileComponent/ProfileComponent";
import style from './ProfilePage.module.css';
import ProfileNoteCreateComponent from "./ProfileNoteCreateComponent/ProfileNoteCreateComponent";
import {connect} from "react-redux";
import ProfileWallElementComponent from "./ProfileWallElementComponent/ProfileWallElementComponent";
import HeaderContainer from "../Header/HeaderContainer";

const ProfilePage = (props) => {

    let wallPostsList = [...props.wallPosts.posts].reverse().map ((post) => {
        return <ProfileWallElementComponent post={post} avatar={props.wallPosts.wallPostAvatar}/>
    } );
    return (
        <div className={style["page-width"]}>
            <div className={style["center-page"]}>
                <HeaderContainer/>
                <div className={style["content-wrapper"]}>
                    <div className="sidebar-block">
                        <SidebarComponent/>
                    </div>
                    <div>
                        <ProfileComponent />
                        <ProfileNoteCreateComponent/>
                        {wallPostsList}
                    </div>
                </div>
            </div>
        </div>
    );
};
const mapStateToProps = (state) => {
    return {
        profile: state.profilePage,
        wallPosts: state.profilePage.wallPosts
    }
};
const mapDispatchToProps = () => {
    return {

    }
};
const ConnectProfilePage = connect(mapStateToProps, mapDispatchToProps)(ProfilePage);

export default ConnectProfilePage;
