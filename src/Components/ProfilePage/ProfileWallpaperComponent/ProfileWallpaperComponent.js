import React from 'react';
import style from './ProfileWallpaperComponent.module.css';


const ProfileWallpaperComponent = (props) => {

    return (
        <div>
            <img className={style["profile-wallpaper"]} src={props.profilePage.wallpaper} alt={props.profilePage.wallpaper}/>
        </div>
    );
};

export default ProfileWallpaperComponent;
