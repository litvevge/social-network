import React from 'react';
import {connect} from "react-redux";
import Header from "./Header";
import {logOut, me} from "../../Reducers/authReducer";

class HeaderContainer extends React.Component {

    componentWillMount() {
        this.props.me();
    };

    render () {
        return <Header {...this.props}/>
    }

}
const mapStateToProps = (state) => {
    return {
        isAuth: state.auth.isLoggedIn,
        userInfo: state.auth.userInfo
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        me: () => {
            dispatch(me());
        },
        logOut: () => {
            dispatch(logOut());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);

