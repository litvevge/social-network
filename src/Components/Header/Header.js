import React from 'react';
import style from './Header.module.css';
import logo from '../../assets/images/logo.png';
import {NavLink} from "react-router-dom";



const Header = (props) => {

    const onLogOutClick = () => {
        debugger;
        props.logOut();
    };
        return (
            <header className={style.header}>
                <div>
                    <img className={style["header-logo"]} src={logo} alt="logo"/>
                </div>
                <div >

                </div>
                <div>
                    {props.isAuth && <div><span className={style["header-logo"]}>{props.userInfo.userName} </span><span onClick={onLogOutClick} >Log out </span></div>}
                    {!props.isAuth && <NavLink className={style["header-logo"]} to='/'>Log in</NavLink>}
                </div>
            </header>
        );
    };

export default Header;
