import React from 'react';
import {NavLink} from 'react-router-dom';
import style from './SidebarComponent.module.css';

const SidebarComponent = () => {
    return (
        <div className={style.sidebar}>
            <ul>
                <li><NavLink to='/profile'>Profile</NavLink></li>
                <li><NavLink to='/dialogs'>Dialogs</NavLink></li>
                <li><NavLink to='/users'>Users</NavLink></li>
                <li><NavLink to='/news'>News</NavLink></li>
                <li><NavLink to='/music'>Music</NavLink></li>
                <li><NavLink to='/settings'>Settings</NavLink></li>
            </ul>
        </div>
    );
};

export default SidebarComponent;
