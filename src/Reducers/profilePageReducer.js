export const ADD_WALL_POST = 'NETWORK/PROFILE/ADD_WALL_POST';
export const CURRENT_POST_TEXT = 'NETWORK/PROFILE/CURRENT_POST_TEXT';


let initialStateForProfilePage = {
    profileInfo: {
        wallpaper: ["https://images1.popmeh.ru/upload/img_cache/2a8/2a8ab7771730d6842b4068f3674b9b03_ce_980x600x0x0_cropped_800x427.jpg"],
        avatar: ["https://imgur.com/I80W1Q0.png"],
        firstsName: "Alex",
        secondName: "Smith",
        birthday: "23 December",
        city: "Minsk",
        education: "BNTU",
        website: "blablabla.com"
    },
    wallPosts: {
        currentWritingMessage: "",
        uidPost: "1",
        posts: ["Hello", "How are you?"],
        wallPostAvatar: ["https://imgur.com/I80W1Q0.png"]
    }
};
const profilePageReducer = (state = initialStateForProfilePage, action) => {
    switch (action.type) {
        case ADD_WALL_POST:
            debugger;
            let newState = {...state};
            newState.wallPosts.posts.push(action.text);
            return newState;
        // case 'CURRENT_POST_TEXT':
        //     let newState1 = {...state};
        //     newState1.wallPosts.currentWritingMessage(action.text);
        //     return newState1;
        default:
            return state;
    }
};
export default profilePageReducer;

export const addWallPost = (text) => {
    return {
        type: ADD_WALL_POST,
        text: text
    }
};

export const setCurrentPostText = (text) => {
    return {
        type: CURRENT_POST_TEXT,
        post: text
    }
};
