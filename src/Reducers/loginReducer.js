import {setIsLogIn} from "./authReducer";
import axiosInstance from './../dal/axios-instance'

const statuses = {
    INIT: 'INIT',
    ERROR: 'ERROR',
    INPROGRESS: 'INPROGRESS',
    SUCCESS: 'SUCCESS',
    CAPTCHAREQUARED: 'CAPTCHAREQUARED'
};

let initialStateForLoginPage = {
    status: statuses.INIT, //'in progress'
    captchaURL: '',
    message: ''
};

// export const REMEMBER_USER = 'NETWORK/LOGIN/REMEMBER';
// export const LOGIN_ONCHANGE = 'NETWORK/LOGIN/LOGIN_ONCHANGE';
// export const PASSWORD_ONCHANGE = 'NETWORK/LOGIN/PASSWORD_ONCHANGE';
// export const CHANGE_STATUS = 'NETWORK/LOGIN/CHANGE_STATUS';
export const SET_STATUS = 'NETWORK/LOGIN/SET_STATUS';
export const SET_MESSAGE = 'NETWORK/LOGIN/SET_MESSAGE';

export const setStatus = (status) => ({type: SET_STATUS, status});
export const setMessage = (message) => ({type: SET_MESSAGE, message});

export const login = (login, password, rememberMe, captcha) => (dispatch) => {

    dispatch(setStatus(statuses.INPROGRESS));

    axiosInstance.post('auth/login', {
        email: login,
        password: password,
        rememberMe: rememberMe
    }).then(res => {
        debugger;
        if (res.data.resultCode === 0) {
            dispatch(setStatus(statuses.SUCCESS));
            dispatch(setIsLogIn(true));
        }
        else {
            dispatch(setStatus(statuses.ERROR));
            dispatch(setMessage(res.data.messages[0]));
        }
    })
};

const loginPageReducer = (state = initialStateForLoginPage, action) => {
    switch (action.type) {
        case SET_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        case SET_MESSAGE: {
            return {
                ...state,
                message: action.message
            }
        }
        default: {
            return state;
        }
    }
};
// //action creators
// export const changeLogin = (login) => {
//     return {
//         type: LOGIN_ONCHANGE,
//         login: login
//     }
// };
//
// export const changePassword = (password) => {
//     return {
//         type: PASSWORD_ONCHANGE,
//         password: password
//     }
// };
//
// export const changeStatus = (status) => {
//     return {
//         type: CHANGE_STATUS,
//         status: status
//     }
//
// };

// export const loginSubmitClick = () => {
//     return (dispatch) => {
//         dispatch(changeStatus('in progress'));
//         setTimeout(() => {
//             dispatch(setLogInToTrue());
//         }, 3000);
//     };
// };

export default loginPageReducer;
