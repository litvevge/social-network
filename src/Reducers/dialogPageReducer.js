export const SET_CURRENT_USER = 'NETWORK/DIALOGS/SET_CURRENT_USER';

let initialStateForDialogPage = {
    dialogs: {
        users: [
            {
                id: 0,
                name: "Alex"
            },
            {
                id: 1,
                name: "Max"
            },
            {
                id: 3,
                name: "John"
            }],
        messages: [{
            id: 4,
            author: "Alex",
            authorId: 1,
            message: "Hi",
            avatar: "https://imgur.com/I80W1Q0.png",
            date: new Date(2019, 11, 10)
        },
            {
                id: 2,
                author: "Gene",
                authorId: 100,
                message: "Hi Hi Hi",
                avatar: "https://imgur.com/I80W1Q0.png",
                date: new Date(2019, 11, 10)
            }],
        currentUserId: 1
    }
};

const dialogPageReducer = (state = initialStateForDialogPage, action) => {
    switch (action.type) {
        case 'SET_CURRENT_USER':
            state.dialogs.currentUserId = action.id;
            return state;
        default:
            return state;
    }
};
export default dialogPageReducer;