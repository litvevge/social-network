import * as axios from "axios";

export const SET_USERS = 'NETWORK/USERS/SET_USERS';
export const SET_STATUS = 'NETWORK/USERS/SET_STATUS';

export const statuses = {
    NOT_INITIOLIZED: 'NOT-INITIALIZED',
    ERROR: 'ERROR',
    INPROGRESS: 'INPROGRESS',
    CAPTCHAREQUARED: 'CAPTCHAREQUARED',
    SUCCESS: 'SUCCESS'
};

let initialStateForLoginPage = {
    status: statuses.NOT_INITIOLIZED,
    items: [],
    totalUsers: 0
};


const usersPageReducer = (state = initialStateForLoginPage, action) => {
    switch (action.type) {
        case SET_USERS:
            return {
                ...state,
                items: action.users
            };
        case SET_STATUS:
            return {
                ...state,
                status: action.status
            };
        default:
            return state;
    }
};

const axiosInstance = axios.create({
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    withCredentials: true
});

export const setUsers = (users) => {
    return {
        type: SET_USERS,
        users: users
    }
};
export const setStatus = (status) => {
    return {
        type: SET_STATUS,
        status: status
    }
};
export const getUsers = () => (dispatch) => {
    dispatch(setStatus(statuses.INPROGRESS));
    axiosInstance.get('users').then(r => {
        dispatch(setStatus(statuses.SUCCESS));
        dispatch(setUsers(r.data.items))
    });
};

export default usersPageReducer;
