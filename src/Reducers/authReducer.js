import axiosInstance from "../dal/axios-instance";

export const IS_LOG_IN = 'NETWORK/AUTH/IS_LOG_IN';
export const SET_USER_INFO = 'NETWORK/AUTH/SET_USER_INFO';

let initialState = {
    isLoggedIn: false,
    userInfo: {
        userId: null,
        name: null,
        avatar: ''
    }
};

export const setIsLogIn = (value) => ({type: IS_LOG_IN, value});
export const setUserInfo = (userId, userName) => ({type: SET_USER_INFO, userId,userName});


export const me = () => (dispatch) => {
    axiosInstance.get('auth/me')
        .then(res => {
            if (res.data.resultCode === 0) {
                dispatch(setIsLogIn(true));
                dispatch(setUserInfo(res.data.data.userId, res.data.data.login));
            }
        })
};

export const logOut = () => (dispatch) => {
    axiosInstance.post('auth/logout')
        .then(res => {
            if (res.data.resultCode === 0) {
                dispatch(setIsLogIn(false));
                dispatch(setUserInfo(null, null));
            }
        })
};


const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_LOG_IN: {
            return {
                ...state,
                isLoggedIn: action.value
            }
        }
        case SET_USER_INFO: {
            return {
                ...state,
                userInfo: {
                    ...state.userInfo,
                    userId: action.userId,
                    userName: action.userName
                }
            }
        }
        default:
            return state;
    }
};

export default authReducer;
